package com.company;

import java.rmi.RemoteException;
import java.util.Vector;

public interface ChatService {
    boolean register(String userName, CallBack callBack) throws RemoteException;

    boolean unregister(String userName) throws RemoteException;

    Vector<String> inform(String line) throws RemoteException;

    boolean communicate(String nick, String text) throws RemoteException;
}
